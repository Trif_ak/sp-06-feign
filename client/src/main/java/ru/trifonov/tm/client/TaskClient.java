package ru.trifonov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.trifonov.tm.dto.TaskDTO;

import java.util.List;

@FeignClient(url = "http://localhost:8080/rest-server/", name = "taskClient")
public interface TaskClient {
    @PostMapping(value = "/task")
    void post(@RequestBody @NotNull final TaskDTO taskDTO);

    @PutMapping(value = "/task")
    void put(@RequestBody @NotNull final TaskDTO taskDTO);

    @Nullable
    @GetMapping(value = "/tasks")
    List<TaskDTO> getAll();

    @Nullable
    @GetMapping(value = "/tasks/{projectId}")
    List<TaskDTO> getAllByProject(@PathVariable @NotNull final String projectId);

    @Nullable
    @GetMapping(value = "/task/{id}")
    TaskDTO get(@PathVariable @NotNull final String id);

    @DeleteMapping(value = "/task/{id}")
    void delete(@PathVariable @NotNull final String id);
}
