package ru.trifonov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.trifonov.tm.dto.ProjectDTO;

import java.util.List;

@FeignClient(url = "http://localhost:8080/rest-server/", name = "projectFeign")
public interface ProjectClient {
    @PostMapping(value = "/project")
    void post(@RequestBody @NotNull final ProjectDTO projectDTO);

    @PutMapping(value = "/project")
    void put(@RequestBody @NotNull final ProjectDTO projectDTO);

    @Nullable
    @GetMapping(value = "/projects")
    List<ProjectDTO> getAll();

    @Nullable
    @GetMapping(value = "/project/{id}")
    ProjectDTO get(@PathVariable @NotNull final String id);

    @DeleteMapping(value = "/project/{id}")
    void delete(@PathVariable @NotNull final String id);
}
