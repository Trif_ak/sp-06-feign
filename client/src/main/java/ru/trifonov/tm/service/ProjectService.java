package ru.trifonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.client.ProjectClient;
import ru.trifonov.tm.dto.ProjectDTO;

import java.util.List;

@Service
public final class ProjectService extends AbstractService implements IProjectService {
    @NotNull private final ProjectClient projectClient;

    @Autowired
    public ProjectService(@NotNull final ProjectClient projectClient) {
        this.projectClient = projectClient;
    }

    @Override
    @SneakyThrows
    public void insert(
            @Nullable final String name, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        projectClient.post(projectDTO);
    }

    @SneakyThrows
    @Override
    public void update(
            @Nullable final String id, @Nullable final String name,
            @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                id,
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        projectClient.put(projectDTO);
    }

    @Override
    public List<ProjectDTO> findAll() {
        @Nullable List<ProjectDTO> projectsDTO = projectClient.getAll();
        if (projectsDTO == null  || projectsDTO.isEmpty()) throw new NullPointerException("Projects not found.");
        return projectsDTO;
    }

    @Override
    public ProjectDTO find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        @Nullable ProjectDTO projectDTO = projectClient.get(id);
        if (projectDTO == null) throw new NullPointerException("Projects not found.");
        return projectDTO;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        projectClient.delete(id);
    }
}