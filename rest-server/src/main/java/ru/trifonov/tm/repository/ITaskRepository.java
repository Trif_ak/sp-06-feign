package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.trifonov.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends CrudRepository<Task, String> {
    @NotNull
    @Override
    List<Task> findAll();

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String projectId);
}
