package ru.trifonov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.CurrentStatus;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class ComparableEntityDTO extends AbstractDTO {
    @NotNull
    protected String projectId = "";

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    protected CurrentStatus status = CurrentStatus.PLANNED;

    @NotNull
    protected Date beginDate = new Date();

    @NotNull
    protected Date endDate = new Date();

    @NotNull
    protected Date createDate = new Date();
}
