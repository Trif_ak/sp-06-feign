package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    void insert(@Nullable ProjectDTO projectDTO);

    void update(@Nullable ProjectDTO projectDTO);

    @NotNull List<ProjectDTO> findAll();

    @NotNull ProjectDTO find(@Nullable String id);

    void delete(@Nullable String id);

    @NotNull ProjectDTO entityToDTO(@NotNull Project project);

    @NotNull Project dtoToEntity(@NotNull ProjectDTO projectDTO);
}
