package ru.trifonov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.dto.TaskDTO;

import java.util.List;

@RestController
public final class TaskController {
    @NotNull
    ITaskService taskService;

    @Autowired
    public TaskController(@NotNull final ITaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/task")
    public void createTaskPost(@RequestBody @Nullable final TaskDTO taskDTO) {
        taskService.insert(taskDTO);
    }

    @PutMapping(value = "/task", produces = MediaType.APPLICATION_XML_VALUE)
    public void taskEditPost(@RequestBody @Nullable final TaskDTO taskDTO){
        taskService.update(taskDTO);
    }

    @NotNull
    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_XML_VALUE)
    public List<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @NotNull
    @GetMapping(value = "/tasks/{projectId}", produces = MediaType.APPLICATION_XML_VALUE)
    public List<TaskDTO> findTaskByProjectId(@PathVariable(name = "projectId") @Nullable final String projectId) {
        return taskService.findAllByProjectId(projectId);
    }

    @NotNull
    @GetMapping("/task/{id}")
    public TaskDTO findById(@PathVariable(name = "id") @Nullable final String id) {
        return taskService.find(id);
    }

    @DeleteMapping("/task/{id}")
    public void delete(@PathVariable(name = "id") @Nullable final String id) {
        taskService.delete(id);
    }
}
